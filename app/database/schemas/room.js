'use strict';

var Mongoose  = require('mongoose');
Mongoose.connect('mongodb://hungvu:lamcao123@ds129831.mlab.com:29831/chatio_social');

/**
 * Each connection object represents a user connected through a unique socket.
 * Each connection object composed of {userId + socketId}. Both of them together are unique.
 *
 */
var RoomSchema = new Mongoose.Schema({
    title: { type: String, required: true },
    connections: { type: [{ userId: String, socketId: String }]}
});

var roomModel = Mongoose.model('room', RoomSchema);

module.exports = roomModel;